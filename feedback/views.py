from rest_framework import mixins
from rest_framework import viewsets
from .models import Feedback
from .serializers import FeedbackSerializer


# Create your views here.


class FeedbackCreateViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    """
    A viewset that provides `retrieve`, `create`, and `list` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.
    """
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer