from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit, ResizeToFill
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.


def directory_path(instance, filename):
    #file will be uploaded to MEDIA_ROOT/<project.slug_header>/<filename>
    if 'slug_title' in instance.__dict__:
        return '{0}/{1}'.format(instance.slug_title, filename)
    return '{0}/{1}'.format(instance.project.slug_title, filename)


class Project(models.Model):
    title = models.CharField(verbose_name="Заголовок",max_length=300, blank=True)
    main_picture = models.ImageField(
        verbose_name="Главное изображение",
        upload_to=directory_path,
        help_text="Загрузите изображение",
        blank=True)
    main_picture_thumbnail = ImageSpecField(
        source='main_picture',
        processors=[ResizeToFill(327, 118)],
        format='JPEG',
        options={'quality': 80})
    slug_title = models.SlugField(max_length=300, blank=True)
    description = RichTextUploadingField(config_name='default', verbose_name="Описание", blank=True)
    link_project = models.URLField(verbose_name="Адрес сайта",blank=True)
    created = models.DateTimeField(verbose_name="Дата", auto_now_add=True)
    category_choices = (
        ("sites", 'Сайты'),
        ("games", 'Игры'),
        ("software", 'Софт'),
    )
    category = models.CharField(
        verbose_name="Категория",
        max_length=30,
        choices=category_choices,
        default="sites",
        blank=True
    )

    def __str__(self):
        return self.slug_title


class Picture(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='pictures')
    screenshot = models.ImageField(
        verbose_name="Скриншоты проекта",
        upload_to=directory_path,
        help_text="Загрузите изображение",
        blank=True)
    screenshot_thumbnail = ImageSpecField(
        source='screenshot',
        processors=[ResizeToFit(width=480,)],
        format='JPEG',
        options={'quality': 80})


class Video(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='videos')
    link_video = models.URLField(verbose_name="Видео", blank=True)