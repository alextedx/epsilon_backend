from django.contrib import admin
from .models import Project, Picture, Video
from modeltranslation.admin import TabbedExternalJqueryTranslationAdmin
from django.utils.html import mark_safe

# Register your models here.


class PictureInline(admin.TabularInline):
    model = Picture

    def get_screenshot(self, obj):
        if obj.screenshot:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.screenshot.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_screenshot.short_description = 'Изображение'
    readonly_fields = ['get_screenshot']


class VideoInline(admin.TabularInline):
    model = Video


class ProjectAdmin(TabbedExternalJqueryTranslationAdmin):

    group_fieldsets = True
    inlines = [
        PictureInline,
        VideoInline,
    ]

    def get_main_picture_thumbnail(self, obj):
        if obj.main_picture_thumbnail:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.main_picture_thumbnail.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_main_picture_thumbnail.short_description = 'Главное изображение'
    readonly_fields = ['get_main_picture_thumbnail']
    list_filter = ['category', 'created', 'title', ]
    search_fields = ['title', 'description', 'link_project',]
    prepopulated_fields = {"slug_title": ("title",)}
    list_display = (
        'title',
        'get_main_picture_thumbnail',
        'link_project',
        'created',
        'category'
    )


admin.site.register(Project, ProjectAdmin)
