from rest_framework import viewsets
#from rest_framework.decorators import api_view, action
#from rest_framework.response import Response
#from rest_framework.reverse import reverse
from .models import Project, Picture, Video
from .serializers import ProjectSerializer, PictureSerializer, VideoSerializer

# Create your views here.

'''
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        "projects": reverse('project-list', request=request, format=format),
    })
'''


class ProjectViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Project.objects.all()
    filter_fields = ('category', 'slug_title')
    serializer_class = ProjectSerializer
    #lookup_field = 'projects'
'''
    @action(detail=False)
    def category(self, request, category="software"):
        """
        Returns a list of projects filtered by category.
        """
        category_projects = Project.objects.filter(category=category).order_by('created')
        page = self.paginate_queryset(category_projects)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            print(category_projects)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(category_projects, many=True)
        return Response(serializer.data)
'''

class PictureViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer


class VideoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
