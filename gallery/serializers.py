from rest_framework import serializers
from .models import Project, Picture, Video


class PictureSerializer(serializers.ModelSerializer):
    screenshot_thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = Picture
        fields = ('screenshot_thumbnail', 'screenshot',)


class VideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Video
        fields = ('link_video',)


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    main_picture_thumbnail = serializers.ImageField(read_only=True)
    pictures = PictureSerializer(many=True, read_only=True)
    videos = VideoSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = (
            'id',
            'url',
            'category',
            'title',
            'slug_title',
            'main_picture_thumbnail',
            'description',
            'link_project',
            'pictures',
            'videos'
        )
